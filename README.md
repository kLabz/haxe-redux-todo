# Haxe React/Redux Todo App

Implementing react-redux's demo from [here](http://redux.js.org/docs/basics/UsageWithReact.html#implementing-container-components) with new features for [haxe-react](https://github.com/massiveinteractive/haxe-react) and [haxe-redux](https://github.com/elsassph/haxe-redux).

* [PR merged] [`@:jsxStatic` for functional components](doc/jsx-static.md)
* [PR pending] [Higher Order Components](doc/redux-hoc.md)
* [PR pending] [Redux thunk](doc/redux-thunk.md)


