import TodoListStore.TodoListState;

typedef ApplicationState = {
	var todoList:TodoListState;
}
